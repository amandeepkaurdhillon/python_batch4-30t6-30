Types of tags

1. Singular tags -- <br>
2. Paired Tags -- <html>Some Content</html>
3. Self closing tags -- <br/> <hr/>


SYNTAX

<tag attname="attValue">